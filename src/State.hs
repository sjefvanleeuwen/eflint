module State where

import Spec

import qualified Data.Map as M

import Data.Aeson hiding (String)

data Meta   = Meta {
              }
              deriving Eq

data State =  State {
                  contents :: M.Map Tagged Meta  -- meta-info about components
              ,   time :: Int
              }
              deriving Eq

create :: Tagged -> State -> State
create te s = s { contents = M.insert te Meta{} (contents s) }

create_all :: [Tagged] -> State -> State
create_all = flip (foldr create)

terminate :: Tagged -> State -> State
terminate te s = s { contents = M.delete te (contents s) }

terminate_all :: [Tagged] -> State -> State
terminate_all = flip (foldr terminate)

holds :: Tagged -> State -> Bool
holds te s = te `M.member` contents s

emptyState = State { contents = M.empty, time = 0 }

increment_time state = state { time = 1 + (time state) }

instance Show State where
  show state = unlines $ 
      [ show_component c
      | (c,m) <- M.assocs (contents state)
      ]

instance ToJSON State where
 toJSON state = toJSON (map TaggedJSON (state_holds state)) 

state_holds :: State -> [Tagged]
state_holds state = [ te | (te, m) <- M.assocs (contents state) ]

get_facts :: Spec -> State -> ([Tagged] -- facts
                              ,[Tagged] -- acts
                              ,[Tagged]) -- duties
get_facts spec s = foldr div ([], [], []) (M.keys (contents s))
  where div te@(t,d) (fs,as,ds) = 
          case fmap kind (find_decl spec d) of 
            Just (Fact _) -> (te:fs,as,ds)
            Just (Act _)  -> (fs,te:as,ds)
            Just (Duty _) -> (fs,as,te:ds)
            Nothing       -> (fs,as,ds)


