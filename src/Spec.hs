{-# LANGUAGE OverloadedStrings #-}

module Spec where

import Data.Foldable (asum)
import Data.List (intercalate, elem)
import Data.Maybe (isJust)
import qualified Data.Map as M
import qualified Data.Set as S

import Data.Aeson hiding (String)

type DomId      = String -- type identifiers
type Tagged     = (Elem, DomId)

data Var        = Var DomId String {- decoration -}
                deriving (Ord, Eq)

data Elem       = String String 
                | Int Int
                | Product [Tagged]
                deriving (Ord, Eq)

data Domain     = AnyString
                | AnyInt
                | Strings [String]
                | Ints [Int]
                | Products [Var]
                | Time
                | External
                deriving (Ord, Eq)

isExternal :: Spec -> Var -> Bool 
isExternal spec x = case find_decl spec (remove_decoration spec x) of
  Nothing -> False
  Just tspec -> case domain tspec of 
    External  -> True
    _         -> False

enumerable :: Spec -> Domain -> Bool
enumerable spec d = case d of
  Strings _     -> True
  Ints _        -> True
  Products vars -> all (enumerable_var spec) vars
  AnyString     -> False
  AnyInt        -> False
  Time          -> False
  External      -> False
  where enumerable_var :: Spec -> Var -> Bool
        enumerable_var spec v = case fmap domain (find_decl spec (remove_decoration spec v)) of
          Nothing   -> False
          Just dom  -> enumerable spec dom


type Arguments  = Either [Term] [Modifier]

data Modifier   = Rename Var Term -- with var instantiated instead as the value of expr
                deriving (Ord, Eq)

data Kind       = Fact FactSpec | Act ActSpec | Duty DutySpec
                deriving (Ord, Eq, Show)

data TypeSpec   = TypeSpec {
                      kind  :: Kind
                    , domain :: Domain
                    , domain_constraint :: Term
                    , derivation :: Maybe Derivation
                    } deriving Show

data Derivation = Dv [Var] Term
                | ExternalValue
                deriving (Ord, Eq, Show)

data FactSpec   = FactSpec {
                      invariant :: Bool -- TODO move to outer AST
                    }
                deriving (Ord, Eq, Show)

data DutySpec   = DutySpec {
                      enforcing_acts :: Maybe [DomId] --TODO move to outer ast
                    , violated_when  :: Maybe Term 
                    }
                deriving (Ord, Eq, Show)

data ActSpec    = ActSpec {
                      condition   :: Term
                    , effects     :: [Effect] 
                    }
                deriving (Ord, Eq, Show)

data Effect     = CAll  [Var] Term
                | TAll  [Var] Term
                deriving (Ord, Eq, Show)

data Spec       = Spec {
                    decls       :: M.Map DomId TypeSpec
                  , aliases     :: M.Map DomId DomId
                  , invariants  :: [DomId]
                  }

initial_spec decls aliases invs = Spec (M.insert "int" int_decl decls) aliases invs

basic :: Spec -> S.Set DomId 
basic spec = M.foldrWithKey op S.empty (decls spec)
  where op d tspec res | isJust (derivation tspec) = res
                       | otherwise                 = S.insert d res

derived :: Spec -> S.Set DomId 
derived spec = M.foldrWithKey op S.empty (decls spec)
  where op d tspec res | isJust (derivation tspec) = S.insert d res
                       | otherwise                 = res

-- type-environment pairs, restricting either:
-- * the components of the initial state (all instantiations of <TYPE> restricted by <ENV>)
--    (can be thought of as the Genesis transition, constructing the Garden of Eden)
-- * the possible actions performed in a state, only the actions <TYPE> are enabled 
--    if they are consistent with <ENV>
type Initialiser= [Effect] 

data Statement  = Action [Var] ActionType (Either Term (DomId, Arguments)) -- foreach-application that should evaluate to exactly one act
                | Query Term

data ActionType = CallAct | AddEvent | RemEvent
                deriving (Ord, Eq, Enum)

type Scenario   = [Statement]

type Subs       = M.Map Var Tagged

data Term       = Not Term
                | And Term Term
                | Or Term Term
                | BoolLit Bool

                | Leq Term Term
                | Geq Term Term
                | Ge Term Term
                | Le Term Term

                | Sub Term Term
                | Add Term Term
                | Mult Term Term
                | Div Term Term
                | IntLit Int

                | StringLit String

                | Eq Term Term
                | Neq Term Term

                | Exists [Var] Term
                | Forall [Var] Term
                | Count [Var] Term
                | Sum [Var] Term 
                | When Term Term 
                | Present Term
                | Violated Term
                | Enabled Term
                | Project Term Var

                | Tag Term DomId -- should perhaps not be exposed to the user, auxiliary
                | Untag Term     -- auxiliary
                | Ref Var
                | App DomId Arguments 
                | CurrentTime
                deriving (Show, Ord, Eq)

data Value      = ResBool Bool
                | ResString String
                | ResInt Int
                | ResTagged Tagged
                 deriving (Eq, Show)

data Type       = TyStrings
                | TyInts
                | TyBool
                | TyTagged DomId
                deriving (Eq, Show)

instance Show ActionType where
  show CallAct = ""
  show AddEvent = "+"
  show RemEvent = "-"

instance Show Elem where
  show v = case v of
    String s    -> show s
    Int i       -> show i
    Product cs  -> "(" ++ intercalate "," (map show_component cs) ++ ")"

instance Show Domain where
  show r = case r of
    External      -> "<External>"
    Time          -> "<TIME>"
    AnyString     -> "<STRING>" 
    Strings ss    -> "<STRING:" ++ intercalate "," (map show ss) ++ ">"
    AnyInt        -> "<INT>"
    Ints is       -> "<INT:" ++ intercalate "," (map show is) ++ ">"
    Products rs   -> "(" ++ intercalate " * " (map show rs) ++ ")"

instance Show Modifier where
  show (Rename dt1 dt2) = show dt1 ++ " = " ++ show dt2

instance Show Var where
  show (Var ty dec) = ty ++ dec

no_decoration :: DomId -> Var
no_decoration ty = Var ty "" 

remove_decoration :: Spec -> Var -> DomId
remove_decoration spec (Var dom _) = chase_alias spec dom 

find_decl :: Spec -> DomId -> Maybe TypeSpec
find_decl spec d = M.lookup (chase_alias spec d) (decls spec)

find_violation_cond :: Spec -> DomId -> Maybe Term
find_violation_cond spec d = case M.lookup (chase_alias spec d) (decls spec) of
  Nothing -> error ("unknown duty: " ++ d)
  Just ts -> case kind ts of 
    Duty ds -> violated_when ds
    _       -> error ("unknown duty: " ++ d)

chase_alias :: Spec -> DomId -> DomId
chase_alias spec d = maybe d id (M.lookup d (aliases spec))

show_arguments :: Arguments -> String
show_arguments (Right mods) = show_modifiers mods
show_arguments (Left xs) = "(" ++ intercalate "," (map show xs) ++ ")"

show_modifiers :: [Modifier] -> String
show_modifiers [] = "()"
show_modifiers cs = "(" ++ intercalate "," (map show cs) ++ ")"

show_projections :: [Var] -> String
show_projections [] = ""
show_projections ds = "[" ++ intercalate "," (map show ds) ++ "]"

show_component :: Tagged -> String
show_component (v,t) = show v ++ ":" ++ t

show_stmt :: Statement -> String
show_stmt (Query t) = "?" ++ show t 
show_stmt (Action xs atype etm) = case xs of 
  [] -> case etm of Left t         -> show atype ++ show t
                    Right (d,mods) -> show atype ++ d ++ show_arguments mods 
  _  -> "(" ++ "Foreach " ++ intercalate "," (map show xs) ++ " : " ++ show_stmt (Action [] atype etm) ++ ")"

valOf :: Tagged -> Elem
valOf (c,t) = c

tagOf :: Tagged -> DomId
tagOf (c,t) = t

substitutions_of :: [Modifier] -> [(Var, Term)]
substitutions_of = map (\(Rename x y) -> (x,y)) 

make_substitutions_of :: [Var] -> Arguments -> [(Var, Term)] 
make_substitutions_of _ (Right mods) = substitutions_of mods
make_substitutions_of xs (Left args) = zip xs args

project :: Tagged -> DomId -> Maybe Tagged
project (Product tvs,_) ty = asum (map try tvs)
  where try tv@(v,ty') | ty == ty' = Just tv
        try _                        = Nothing
project _ _ = Nothing

-- environments

emptySubs :: Subs
emptySubs = M.empty

-- | right-biased
subsUnion :: Subs -> Subs -> Subs
subsUnion = flip M.union

subsUnions :: [Subs] -> Subs
subsUnions = foldr subsUnion M.empty

{-
subsUnify :: Subs -> Subs -> Bool 
subsUnify e1 = M.foldrWithKey op True 
  where op k v res | Just v' <- M.lookup k e1, v /= v' = False
                   | otherwise                         = res
-}

-- functions related to partial instantiation (refinement)

type Refiner = M.Map DomId Domain

refine_specification :: Spec -> Refiner -> Spec
refine_specification spec rm = 
  spec { decls = M.foldrWithKey reinserter (decls spec) (decls spec) }
  where reinserter k tspec sm' = case M.lookup k rm of 
          Nothing -> sm'
          Just d  -> case (d, domain tspec) of 
            (_, External)             -> sm''
            (Strings ss, AnyString)   -> sm''
            (Strings ss, Strings ss') 
              | all (`elem` ss') ss   -> sm''
            (Ints is, AnyInt)         -> sm''
            (Ints is, Ints is')
              | all (`elem` is') is   -> sm''
            _ -> sm'
            where sm'' = M.insert k (tspec {domain = d}) sm'

int_decl :: TypeSpec
int_decl = TypeSpec {  kind = Fact (FactSpec False)
                    ,  domain = AnyInt
                    ,  domain_constraint = BoolLit True
                    ,  derivation = Nothing } 
newtype TaggedJSON = TaggedJSON Tagged

instance ToJSON TaggedJSON where
  toJSON (TaggedJSON te@(v,d)) = case v of 
    String s    -> object [ "fact-type" .= toJSON d, "value" .= toJSON s, "textual" .= toJSON (show_component te) ]
    Int i       -> object [ "fact-type" .= toJSON d, "value" .= toJSON i, "textual" .= toJSON (show_component te)  ]
    Product tes -> object [ "fact-type" .= toJSON d, "arguments" .= toJSON (map TaggedJSON tes), "textual" .= toJSON (show_component te) ]


