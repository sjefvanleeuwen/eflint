{-# LANGUAGE LambdaCase, TupleSections #-}

module Eval where

import Spec
import State

import Control.Monad
import Control.Applicative
import Text.Read (readMaybe)

import qualified Data.Map as M

data M_Subs a = M_Subs { runSubs :: Spec -> State -> Subs -> IO (State, [a]) }

nd :: [a] -> M_Subs a
nd vals = M_Subs $ \spec state subs -> return (state, vals)

bind :: Var -> Tagged -> M_Subs a -> M_Subs a
bind k v m = M_Subs $ \spec state -> runSubs m spec state . M.insert k v

scope_var :: Var -> M_Subs a -> M_Subs a
scope_var x m = do 
  te <- every_valid_subs x
  bind x te m

get_dom :: DomId -> M_Subs (Domain, Term)
get_dom d = M_Subs $ \spec state subs -> case find_decl spec d of
                                           Nothing    -> error ("undeclared type: " ++ d)
                                           Just tspec -> return (state, [(domain tspec, domain_constraint tspec)])

get_time :: M_Subs Int
get_time = M_Subs $ \spec state subs -> return (state, [time state])

get_external_instance :: DomId -> M_Subs Elem
get_external_instance dom = M_Subs $ \spec state subs -> do
  let inss = map fst $ filter ((dom==) . snd) $ (M.keys (contents state))
  case null inss of 
    False -> return (state, inss)
    True -> do
      putStr (dom ++ " ? \n>")
      get_elems state []
      where get_elems s acc = do 
              str <- getLine
              case str of 
                "" -> putStrLn "" >> return (s,acc)
                _  -> let elem = case readMaybe str of
                                    Just i  -> Int i
                                    Nothing -> String str
                       in get_elems (create (elem, dom) s) (elem:acc)

get_subs :: M_Subs Subs
get_subs = M_Subs $ \spec state subs -> return (state, [subs])

modify_subs :: (Subs -> Subs) -> M_Subs a -> M_Subs a
modify_subs mod m = M_Subs $ \spec state -> runSubs m spec state . mod 

modify_state :: (State -> State) -> M_Subs ()
modify_state mod = M_Subs $ \spec state subs -> return (mod state, [()])

get_spec :: M_Subs Spec 
get_spec = M_Subs $ \spec state subs -> return (state, [spec])

get_state :: M_Subs State
get_state = M_Subs $ \spec state subs -> return (state, [state])

instance Functor M_Subs where
  fmap  = liftM 

instance Applicative M_Subs where
  pure  = return
  (<*>) = ap

instance Monad M_Subs where
  return a  = M_Subs $ \spec state subs -> return (state, [a])
  (>>=) m f = M_Subs $ \spec state subs -> do 
                (state', as) <- runSubs m spec state subs
                let op (state'', res) a = fmap (++res) <$> runSubs (f a) spec state'' subs
                foldM op (state', []) as 

instance Alternative M_Subs where
  empty     = M_Subs $ \spec state subs -> return (state,[])
  m1 <|> m2 = M_Subs $ \spec state subs -> do
                        (state', xs) <- runSubs m1 spec state subs 
                        (state'',ys) <- runSubs m2 spec state' subs
                        return (state'', xs ++ ys)

instance MonadPlus M_Subs where

instantiate_domain :: DomId -> Domain -> M_Subs Elem
instantiate_domain d dom = case dom of
  External      -> get_external_instance d
  Time          -> get_time >>= \time -> nd [ Int i | i <- [0..time]]
  AnyString     -> error "incapable of instantiating to arbitrary Strings"
  AnyInt        -> error "incapable of instantiating to arbitrary Ints"
  Strings ss    -> nd [ String s | s <- ss ]
  Ints is       -> nd [ Int i    | i <- is ]
  Products xs   -> Product <$> sequence (map every_valid_subs xs)

substitute_var :: Var -> M_Subs Tagged
substitute_var x = do
  spec <- get_spec
  subs <- get_subs
  case M.lookup x subs of
    Just te@(v,d') -> return (v, remove_decoration spec x)
    Nothing        -> error ("no substitution for : " ++ show x)

every_valid_subs :: Var -> M_Subs Tagged
every_valid_subs x = do
    spec <- get_spec
    let d = remove_decoration spec x
    (dom, _) <- get_dom d 
    if enumerable spec dom then generate_instances d
                           else every_available_subs d
 where generate_instances d = do  
          (dom, dom_filter) <- get_dom d 
          e <- instantiate_domain d dom
          (case (dom, e) of 
              (Products xs, Product args) -> modify_subs (`subsUnion` (M.fromList (zip xs args)))
              _                           -> id) $ checkTrue (eval dom_filter)
          return (e,d)
       every_available_subs d = do
          state <- get_state 
          nd [ te | te@(v,d') <- M.keys (contents state), d' == d ]

-- ## Dynamic semantics

results :: M_Subs a -> M_Subs [a]
results n = M_Subs $ \spec state subs -> fmap (:[]) <$> runSubs n spec state subs 

is_in_state :: Tagged -> M_Subs Bool
is_in_state te@(_,d) = M_Subs $ \spec state subs -> case join (fmap derivation (find_decl spec d)) of
  Just ExternalValue -> case M.lookup te (contents state) of
                          Nothing -> do putStrLn ("Does fact " ++ show_component te ++ " hold?")
                                        readLn >>= \b -> 
                                          return (create te state, [b])
                          Just m  -> return (state,[True])
  _                  -> return (state, [holds te state])

is_violated :: Tagged -> M_Subs Bool
is_violated te@(_,d) = (&&) <$> is_in_state te <*> violation_condition te
 where violation_condition te = do 
        spec <- get_spec 
        eval_violation_condition te (find_violation_cond spec d)

eval_violation_condition :: Tagged -> Maybe Term -> M_Subs Bool
eval_violation_condition te@(v,d) mcond = do
  (dom, _) <- get_dom d
  let Products xs = dom
      Product args = v
  modify_subs (`subsUnion` (M.fromList (zip xs args))) 
    (whenBool (eval (maybe (BoolLit False) id mcond)) return)

is_enabled :: Tagged -> M_Subs Bool
is_enabled te@(v,d) = is_in_state te >>= \case
  False -> return False
  True  -> get_kind d >>= \case 
    Act aspec -> do
      (dom, _) <- get_dom d
      let Products xs = dom
      let Product args = v
      modify_subs (`subsUnion` (M.fromList (zip xs args))) 
        (not . null <$> results (checkTrue (eval (condition aspec))))
    _ -> return False

get_kind :: DomId -> M_Subs Kind
get_kind d = M_Subs $ \spec state subs -> return . (state,) $ maybe [] (:[]) (find_kind spec d) 
  where find_kind :: Spec -> DomId -> Maybe Kind 
        find_kind spec d = fmap kind (find_decl spec d)

whenBool :: M_Subs Value -> (Bool -> M_Subs a) -> M_Subs a
whenBool m f = m >>= \case ResBool b -> f b
                           _         -> empty

checkTrue :: M_Subs Value -> M_Subs () 
checkTrue m = m >>= \case ResBool True -> return () 
                          _            -> empty

checkFalse :: M_Subs Value -> M_Subs () 
checkFalse m = m >>= \case ResBool False  -> return () 
                           _              -> empty


whenInt :: M_Subs Value -> (Int -> M_Subs a) -> M_Subs a
whenInt m f = m >>= \case ResInt v -> f v
                          _        -> empty

whenInts :: M_Subs Value -> ([Int] -> M_Subs a) -> M_Subs a
whenInts m f = results m >>= sequence . map (flip whenInt return . return) >>= f

whenTagged :: M_Subs Value -> (Tagged -> M_Subs a) -> M_Subs a
whenTagged m f = m >>= \case ResTagged v  -> f v
                             _            -> empty

whenTaggedHolds m f = m >>= \case ResTagged v -> is_in_state v >>= \case
                                                   True  -> f v
                                                   False -> empty
                                  _           -> empty

whenString :: M_Subs Value -> (String -> M_Subs a) -> M_Subs a
whenString m f = m >>= \case ResString s  -> f s
                             _            -> empty

eval :: Term -> M_Subs Value
eval t0 = case t0 of
  CurrentTime -> ResInt <$> get_time
  StringLit s -> return (ResString s)
  BoolLit b   -> return (ResBool b)
  IntLit i    -> return (ResInt i)
  Ref x       -> ResTagged <$> substitute_var x
  App d params-> do (dom,dom_filter) <- get_dom d
                    case dom of 
                      Products xs -> do 
                        let replacements = make_substitutions_of xs params
                        tes <- mapM (\(x,t) -> whenTagged (eval t) (return . (x,))) replacements
                        args <- modify_subs (`subsUnion` M.fromList tes) (mapM substitute_var xs)
                        modify_subs (`subsUnion` (M.fromList (zip xs args))) $ do
                          checkTrue (eval dom_filter)
                          return (ResTagged (Product args, d))
                      External | null params -> return (ResTagged (Product [], d))
                      _           -> error ("application of primitive: " ++ d)
  Tag t d     -> eval t >>= flip tag d
  Untag t     -> eval t >>= untag
  Not t       -> whenBool (eval t) $ \b -> return (ResBool (not b))
  And t1 t2   -> whenBool (eval t1) $ \case 
                    False -> return (ResBool False)
                    True  -> whenBool (eval t2) (return . ResBool) 
  Or  t1 t2   -> whenBool (eval t1) $ \case 
                    True  -> return (ResBool True)
                    False -> whenBool (eval t2) (return . ResBool)
--  And t1 t2   -> whenBool (eval t1) $ \b1 -> whenBool (eval t2) (\b2 -> return (ResBool (and [b1,b2])))
--  Or  t1 t2   -> whenBool (eval t1) $ \b1 -> whenBool (eval t2) (\b2 -> return (ResBool (or [b1,b2])))
  Leq  t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResBool (v1 <= v2)))
  Le   t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResBool (v1 < v2)))
  Geq  t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResBool (v1 >= v2)))
  Ge   t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResBool (v1 > v2)))
  Mult  t1 t2 -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResInt (v1 * v2)))
  Div  t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResInt (v1 `div` v2)))
  Sub  t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResInt (v1 - v2)))
  Add  t1 t2  -> whenInt  (eval t1) $ \v1 -> whenInt (eval t2) (\v2 -> return (ResInt (v1 + v2)))
  Eq  t1 t2   -> ((ResBool .) . (==)) <$> eval t1 <*> eval t2
  Neq t1 t2   -> ((ResBool .) . (/=)) <$> eval t1 <*> eval t2

  Sum xs t1   -> ResInt . sum <$> foreach xs (whenInt (eval t1) return)
  Count xs t1 -> ResInt . length <$> foreach xs (eval t1)
  When t1 t2  -> -- order matters because of constraint that equal variables have equal instantiations
                 -- however, with renaming some of these constraints can be lifted
                 -- this modification propagates in the same order as the evaluation order
                 eval t1 >>= \v1 -> whenBool (eval t2) (\case True -> return v1
                                                              False -> empty)
                 {- whenBool (eval t2) $ \case  True  -> eval t1 
                                              False -> empty-}
  Present t1  -> whenTagged (eval t1) (\v -> ResBool <$> is_in_state v)
  Violated t1 -> whenTagged (eval t1) (\v -> ResBool <$> is_violated v)
  Enabled t1  -> whenTagged (eval t1) (\v -> ResBool <$> is_enabled v)
  Exists xs t -> ResBool . not . null <$> results (foldr scope_var (checkTrue (eval t)) xs) 
  Forall xs t -> ResBool . and <$> results (foldr scope_var (whenBool (eval t) return) xs)

  Project t x -> whenTagged (eval (t)) $ \te@(e,d) -> case e of 
                    Product tes -> do (dom, _) <- get_dom d
                                      case dom of Products rs | length tes == length rs -> 
                                                    case elemIndex x rs of
                                                      Nothing -> empty
                                                      Just j  -> return (ResTagged (tes !! j))
                                                  _ -> empty
                    _ -> empty
    where elemIndex x rs = msum (zipWith op [0..] rs)
            where op j y | x == y    = Just j
                         | otherwise = Nothing

foreach :: [Var] -> M_Subs a -> M_Subs [a]
foreach xs m = results (foldr scope_var m xs)

tag :: Value -> DomId -> M_Subs Value
tag (ResInt i) d        = return $ ResTagged (Int i,d)
tag (ResString s) d     = return $ ResTagged (String s, d)
tag (ResTagged (v,_)) d = return $ ResTagged (v,d)
tag (ResBool b) _       = empty

untag :: Value -> M_Subs Value
untag (ResTagged (Int i, d))      = return $ ResInt i
untag (ResTagged (String s, d))   = return $ ResString d
untag (ResTagged (Product _, _))  = empty
untag (ResBool _)                 = empty
untag (ResInt _)                  = empty
untag (ResString _)               = empty

