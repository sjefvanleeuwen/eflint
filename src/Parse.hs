{-# LANGUAGE TupleSections #-}

module Parse where

import Spec

import GLL.Combinators hiding (many, some, IntLit, BoolLit, StringLit)
import Text.Regex.Applicative hiding ((<**>), optional)

import Data.Char (isLower)
import Data.Either (lefts, rights)
import qualified Data.Map as M

flint_lexer :: String -> [Token]
flint_lexer = lexer lexer_settings

flint_parser :: BNF Token a -> String -> [a]
flint_parser p = parse p . flint_lexer 

lexer_settings :: LexerSettings
lexer_settings = emptyLanguage {
    identifiers = types
  , keywords =  ["!?","||", "&&", "<=", ">=", "..", "True", "False", "Sum", "==", "!=", "When", "Where","Holds when",  "Holds", "Present when", "Present", "Count", "Union", "Enabled", "Violated when", "Violated"
                , "Atom", "String", "Int", "Time", "Current Time"
                , "Exists", "Forall", "Foreach"
                , "Act", "Physical", "External", "Fact", "Invariant", "Predicate", "Duty", "Actor", "Holder", "Claimant", "Recipient", "Related to", "Conditioned by", "Creates", "Terminates", "Terminated by", "With" , "Identified by", "Derived from", "Derived externally", "Enforced by"
                , "Do", "Placeholder", "For" 
                , "CreateEvent", "TerminateEvent"
                , "#", "##", "###", "####"
                ]
  , keychars = ['[', ']', '(', ')', '!', ',', '\'', '+', '-', '*', '/', '.', '=', '>', '<', ':', '?']
  } 
  where types = (concat .) . (:) <$>  
                word <*> many ((++) <$> (concat <$> some ((:[]) <$> psym (== ' '))) <*> word) <|> 
                (\id -> "[" ++ id ++ "]") <$ sym '[' <*> many internal <* sym ']' <|>
                (\id -> "<" ++ id ++ ">") <$ sym '<' <*> act_or_duty <* sym '>'
          where word = (\c ss -> c:concat ss) <$> psym isLower <*> many ((:[]) <$> psym isLower <|> hyphen)
                  where hyphen = (\c1 c2 -> [c1,c2]) <$> sym '-' <*> psym isLower 
                act_or_duty = (\id -> "<" ++ id ++ ">") <$ sym '<' <*> many internal <* sym '>'
                              <|> (:) <$> psym (/= '<') <*> many internal 
                internal = psym (\c -> not (c `elem` "]>+"))

parse_value_expr :: String -> [Term]
parse_value_expr = parse value_expr . flint_lexer

value_expr :: BNF Token Term
value_expr = "value-expr"
  <::=  BoolLit True <$$ keyword "True"
  <||>  BoolLit False <$$ keyword "False"
  <||>  When <$$> value_expr <** keyword_when <**> value_expr
  
  <||>  Or <$$> value_expr <** keyword "||" <<<**> value_expr
  <||>  And <$$> value_expr <** keyword "&&" <<<**> value_expr

  <||>  Or <$$> value_expr <** keyword "||" <<<**> value_expr
  <||>  And <$$> value_expr <** keyword "&&" <<<**> value_expr

  <||>  Eq <$$> value_expr <** keyword "==" <**> value_expr
  <||>  Neq <$$> value_expr <** keyword "!=" <**> value_expr

  <||>  Leq <$$> value_expr <** keyword "<="  <<<**> value_expr
  <||>  Geq <$$> value_expr <** keyword ">="  <<<**> value_expr
  <||>  Le  <$$> value_expr <** keychar '<'  <<<**> value_expr
  <||>  Ge  <$$> value_expr <** keychar '>' <<<**> value_expr
  <||>  Sub <$$> value_expr <** keychar '-' <**>>> value_expr 
  <||>  Add <$$> value_expr <** keychar '+' <**>>> value_expr 
  <||>  Mult <$$> value_expr <** keychar '*' <**>>> value_expr 
  <||>  Div <$$> value_expr <** keychar '/' <**>>> value_expr 
  <||>  Not <$$ keychar '!' <**> value_expr 

  <||>  keyword "Sum" **> foreach Sum
  <||>  keyword "Count" **> foreach Count 

  <||>  IntLit <$$> int_lit
  <||>  StringLit <$$> atom 
  <||>  Ref <$$> var 
  <||>  application App
  <||>  Project <$$> value_expr <** keychar '.' <**> (var <||> parens var)
  <||>  Tag <$$> value_expr <** keychar ':' <**> id_lit 

  <||>  parens (Exists <$$ keyword "Exists" <**> multipleSepBy1 var (keychar ',') <** keychar ':' <**> value_expr)
  <||>  parens (Forall <$$ keyword "Forall" <**> multipleSepBy1 var (keychar ',') <** keychar ':' <**> value_expr)
  <||>  Present <$$ keyword "Present" <**> value_expr
  <||>  Present <$$ keyword "Holds" <**> value_expr
  <||>  Violated <$$ keyword "Violated" <**> value_expr
  <||>  Enabled <$$ keyword "Enabled" <**> value_expr
  <||>  parens value_expr
  <||>  CurrentTime <$$ keyword "Current Time"

keyword_when :: BNF Token String
keyword_when = "when-or-where" <:=> keyword "Where" <||> keyword "When"

parse_type_expr :: String -> [Domain]
parse_type_expr = parse type_expr . flint_lexer

var :: BNF Token Var
var = "decorated-type-lit"
  <:=> Var <$$> id_lit <**> decoration

atom :: BNF Token String
atom = "atom" <:=> string_lit <||> alt_id_lit

decoration :: BNF Token String 
decoration = "decoration"
  <:=> make_f <$$> optional int_lit <**> multiple (keychar '\'')
  where make_f mi str = maybe "" show mi ++ str

arguments :: BNF Token Arguments 
arguments = "arguments"
  <:=> parens ( Right <$$> multipleSepBy modifier (keychar ',')
           <||> Left <$$> multipleSepBy1 value_expr (keychar ',') )

modifier :: BNF Token Modifier
modifier = "modifier"
  <:=> Rename <$$> var <** keychar '=' <**> value_expr 

fact_attributes :: BNF Token [String]
fact_attributes = "fact-attribute" 
          <:=> keyword "TerminateEvent" **> multipleSepBy1 id_lit (keychar ',')
          <||> keyword "CreateEvent" **> multipleSepBy1 id_lit (keychar ',')
          <||> keyword "Event" **> multipleSepBy1 id_lit (keychar ',')

type_expr :: BNF Token Domain
type_expr = "type-expr"
  <::=  Products . (:[]) <$$> var 
  <||>  Strings <$$> manySepBy2 atom (keychar '+' <||> keychar ',')
  <||>  Ints <$$> manySepBy2 int_lit (keychar '+' <||> keychar ',')
  <||>  Products <$$> manySepBy2 var (keychar '*')
  <||>  parens (Products <$$> manySepBy2 var (keychar '*'))
  <||>  Ints . (:[]) <$$> int_lit
  <||>  ints_from_domain <$$> int_lit <** keyword ".." <**> int_lit
  <||>  Strings . (:[]) <$$> atom 
  <||>  strings_from_domain <$$> char_lit <** keyword ".." <**> char_lit
  <||>  AnyString <$$ keyword "String"
  <||>  AnyString <$$ keyword "Atom"
  <||>  AnyInt <$$ keyword "Int"
  <||>  Time <$$ keyword "Time"
  where ints_from_domain :: Int -> Int -> Domain 
        ints_from_domain min max = Ints $ [min..max]

        strings_from_domain :: Char -> Char -> Domain
        strings_from_domain min max = Strings $ map (:[]) [min..max]


-- parsing frame specifications
parse_component :: BNF Token a -> String -> Either String a
parse_component p str = fmap head (parseWithOptionsAndError [maximumErrors 1] p (flint_lexer str))

flint :: BNF Token (Spec, Refiner, Initialiser, Scenario)
flint = "flint" <:=> (,,,) <$$ optional (keyword "#") <**>
                                frames <** keyword "##" <**>
                                refiner <** keyword "###" <**>
                                initialiser <**> 
                                optionalWithDef (keyword "####" **> scenario) []

parse_flint = parse_component flint

parse_frames = parse_component frames

frames :: BNF Token Spec
frames = "frames" <:=> handle_directives <$$> multiple1 (Left <$$> frame <||> Right <$$> directive)
  where handle_directives phrases = 
          initial_spec (M.fromList (lefts phrases)) (M.fromList (rights phrases)) []

directive :: BNF Token (DomId, DomId)
directive = "directive" <:=> (,) <$$ keyword "Placeholder" <**> id_lit <** keyword "For" <**> id_lit

frame :: BNF Token (DomId, TypeSpec)
frame = "frame" <:=> fact <||> duty <||> act
  where fact = make_fact False <$$ 
                keyword "Fact" <**> id_lit <**>
                optionalWithDef (keyword "Identified by" **> type_expr) AnyString <**>
                domain_constraint <**> 
                optional derivation_from <**
                optional fact_attributes
          <||> make_pred False <$$ keyword "Predicate" <**> id_lit <**
                keyword "When" <**> value_expr 
          <||> make_inv <$$ keyword "Invariant" <**> id_lit <** keychar ':' <**> value_expr
--          <||> make_external <$$ keyword "External" <**> id_lit 
          where make_fact inv ty dom dom_filter mexpr = 
                  (ty, TypeSpec  { kind = Fact (FactSpec inv)
                                 , domain = dom
                                 , domain_constraint = dom_filter
                                 , derivation = fmap ($ ty) mexpr })
                make_pred inv ty expr = make_fact inv ty (Products []) (BoolLit True) (Just (mk_present_when expr))
                make_inv ty expr = make_pred True ty expr 
                make_external ty = make_fact False ty (Strings [ty]) (BoolLit True) (Just (const ExternalValue))

        act = make_act <$$ 
                keyword "Act" <**> id_lit <** optional (keyword "With") <**>
                actor <**> (Just <$$> recipient) <**> objects <**> domain_constraint <**> precon <**> 
                postcon <**> optional derivation_from <**
                optional fact_attributes 
          <||> make_act <$$
                keyword "Physical" <**> id_lit <** optional (keyword "With") <**>
                actor <**> optional recipient <**> objects <**> domain_constraint <**> precon <**> postcon <**> 
                satisfy (Just (\d -> Dv [Var d ""] (Ref (Var d ""))))
          where make_act ty act mrec attrs dom_filter conds effs ot = (ty, TypeSpec {
                  kind = Act (ActSpec {condition = foldr And (BoolLit True) conds, effects = effs} ),
                  domain = Products (act:((maybe [] (:[]) mrec)++attrs)), 
                  domain_constraint = dom_filter,
                  derivation = fmap ($ ty) ot } )
  
        actor = keyword "Actor" **> var 
        recipient = keyword "Recipient" **> var
        objects = optionalWithDef (keyword "Related to" **> multipleSepBy1 var (keychar ',')) []
        domain_constraint = optionalWithDef (keyword_when **> value_expr) (BoolLit True)
        precon = optionalWithDef (keyword "Conditioned by" **> multipleSepBy1 value_expr (keychar ',')) 
                                 [BoolLit True] 
        cpost  = optionalWithDef (keyword "Creates" **> multipleSepBy1 (effect True) (keychar ',')) [] 
        tpost  = optionalWithDef (keyword "Terminates" **> multipleSepBy1 (effect False) (keychar ',')) []
        postcon = (++) <$$> cpost <**> tpost <||> (++) <$$> tpost <**> cpost

        duty = make_duty <$$
                keyword "Duty" <**> id_lit <** optional (keyword "With") <**
                keyword "Holder" <**> var <**
                keyword "Claimant" <**> var <**> 
                objects <**> domain_constraint <**> optional derivation_from <**>
                optional (keyword "Violated when" **> value_expr) <**>
                optional (keyword "Enforced by" **> multipleSepBy1 id_lit (keychar ',')) <**
                optional fact_attributes 
          where make_duty ty hold claim attrs dom_filter ot ve eacts = (ty, TypeSpec {
                  domain = Products (hold:claim:attrs),
                  domain_constraint = dom_filter,
                  kind = Duty (DutySpec { violated_when = ve, enforcing_acts = eacts}), 
                  derivation = fmap ($ ty) ot } )

effect :: Bool -> BNF Token Effect
effect dir = identifier 
  <:=> cons_one <$$> value_expr 
  <||> foreach cons_all
  where identifier | dir        = "create-effect"
                   | otherwise  = "terminate-effect" 
        cons_one   | dir        = CAll []
                   | otherwise  = TAll []
        cons_all   | dir        = CAll
                   | otherwise  = TAll

application :: (DomId -> Arguments -> a) -> BNF Token a
application cons = "application" <:=> cons <$$> id_lit <**> arguments

foreach :: ([Var] -> Term -> a) -> BNF Token a
foreach cons = "foreach"
  <:=> parens (cons <$$ keyword "Foreach" <**> multipleSepBy1 var (keychar ',') 
                         <** keychar ':' <**> value_expr )

derivation_from :: BNF Token (DomId -> Derivation)
derivation_from = "derivation" 
  <:=> const . Dv [] <$$ keyword "Derived from" <**> value_expr
  <||> const ExternalValue <$$ keyword "Derived externally"
  <||> mk_present_when <$$ keyword_present_when <**> value_expr
  <||> keyword "Derived from" **> foreach (\xs t d -> Dv xs t)

mk_present_when :: Term -> DomId -> Derivation
mk_present_when t d = Dv [] (When (App d (Right [])) t)

keyword_present_when :: BNF Token String
keyword_present_when = "present-when"
  <:=> "Holds when" <$$ keyword "Present" <** keyword "When" 
  <||> "Holds when" <$$ keyword "Holds" <** keyword "When" 
  <||> "Holds when" <$$ keyword "Present when"
  <||> keyword "Holds when"

-- parsing refiner specifications
parse_refiner :: String -> Either String Refiner
parse_refiner =  parse_component refiner

refiner :: BNF Token Refiner
refiner = "refinement" <:=> M.fromList <$$> multiple refine

refine :: BNF Token (DomId, Domain)
refine = "refine" <:=> (,) <$$ keyword "Fact" <**> id_lit <** keyword "Identified by" <**> type_expr

-- parsing initial state specifications
parse_initialiser :: String -> Either String Initialiser
parse_initialiser = parse_component initialiser 

initialiser :: BNF Token Initialiser
initialiser = "initial state" <:=> multiple (initial <** keychar '.') 
  where initial = "initial-statement" 
          <:=>  foreach CAll
          <||>  CAll [] <$$> value_expr

-- parsing scenario specifications
parse_scenario :: String -> Either String Scenario
parse_scenario = parse_component scenario
 
scenario :: BNF Token Scenario
scenario = "scenario" <:=> multiple statement

parse_statement :: String -> Either String Statement
parse_statement = parse_component statement

statement :: BNF Token Statement
statement = "statement"
  <:=> ($) <$$> actioner <**> maybe_action <** keychar '.'
  <||> Query <$$ keychar '?' <**> value_expr <** keychar '.'
  <||> Query . Not <$$ keyword "!?" <**> value_expr <** keychar '.'
  where actioner = "actioner" 
          <:=> (\(xs, d, ms) -> Action xs CallAct (Right (d,ms)))  <$$ optional (keychar '!')
          <||> (\(xs, d, ms) -> Action xs AddEvent (Right (d,ms))) <$$ keychar '+' 
          <||> (\(xs, d, ms) -> Action xs RemEvent (Right (d,ms))) <$$ keychar '-'
        maybe_action = "action-statement" 
            <:=> application ([],,)
            <||> parens ((\xs (d,args) -> (xs,d,args)) <$$ keyword "Foreach" <**> (multipleSepBy1 var (keychar ',')) <** keychar ':' <**> application (,))
                          
