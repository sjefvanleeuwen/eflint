{-# LANGUAGE LambdaCase, TupleSections, DeriveGeneric, OverloadedStrings #-}

module Sim where

import Spec
import State
import Eval 
import Saturation

import Control.Monad
import Control.Applicative

import Data.Aeson hiding (String)
import qualified Data.Aeson as JSON
import qualified Data.Map as M
import qualified Data.IntMap as IM
import qualified Data.Set as S
import Data.Maybe (fromJust, isNothing) 

import GHC.Generics

run :: Spec -> Refiner -> Initialiser -> IO ()
run spec refiner initialiser = run_scenario spec refiner initialiser []

run_scenario :: Spec -> Refiner -> Initialiser -> Scenario -> IO ()
run_scenario spec' refiner initialiser scenario 
  = make_initial_state spec initialiser >>= \state -> steps scenario spec state
  where spec = refine_specification spec' refiner

test_scenario :: Spec -> Refiner -> Initialiser -> Scenario -> Bool -> IO TestResult 
test_scenario spec' refiner initialiser scenario positive_test = do 
  state <- make_initial_state spec initialiser
  (state', tr) <- init_test_result spec state
  tester scenario spec state' positive_test tr 
  where spec = refine_specification spec' refiner

init_test_result :: Spec -> State -> IO (State, TestResult)
init_test_result spec state = do
  (state, powers, duties) <- normative_positions spec state
  (state, breaches) <- test_invariants spec state
  return (state, empty_test_result spec state powers duties breaches)


make_initial_state :: Spec -> Initialiser -> IO State
make_initial_state spec initialiser = do
  (state, res) <- runSubs (mapM_ op initialiser) spec emptyState emptySubs
  case res of 
    []  -> error "failed to initialise state"
    [_] -> saturate spec state
    _   -> error "non-deterministic state initialisation"
  where op eff = case eff of 
          CAll xs t -> foreach xs (whenTagged (eval t) return) >>= \tes ->
                          modify_state $ create_all tes
          TAll xs t -> foreach xs (whenTagged (eval t) return) >>= \tes ->
                          modify_state $ terminate_all tes 

steps :: Scenario -> Spec -> State -> IO () -- returns the final state
steps scenario spec state = print_state state >> steps' scenario spec state
 where steps' scenario spec state = do
        (acts, scenario') <- select_step_options scenario spec state 
        case acts of 
          []  -> case scenario of 
                    []        -> putStrLn "No more actions to perform..."
                    (stmt:_)  -> putStrLn ("No more actions to perform, restricted by: " ++ show_stmt stmt) 
          [Left (QueryInfo True)] -> steps scenario' spec state
          [Left (QueryInfo False)] -> do putStrLn ("Query at step " ++ show (time state + 1) ++ " failed")
                                         steps scenario' spec state
          _   -> do
            selection <- make_choice spec (concatMap (either (const []) (:[])) acts)
            case selection of 
              Nothing  -> return ()
              Just act -> update_state spec state act >>= steps scenario' spec 

update_state :: Spec -> State -> ActionInfo -> IO State
update_state spec state act = rebase_and_sat spec (increment_time (fire_act spec state act))

test_single_statement :: Statement -> Spec -> State -> TestResult -> IO TestResult
test_single_statement s spec state = tester [s] spec state True 

-- | Quiet means test successful
tester :: Scenario -> Spec -> State -> Bool -> TestResult -> IO TestResult
tester [] spec state positive_test test_result 
  | positive_test  = return test_result
  | otherwise      = return (test_fail (test_err "Last step executed successfully, unexpectedly" test_result))
tester scenario spec state positive_test test_result = do
  (options, rest) <- select_step_options scenario spec state
  case options of
    [] | positive_test || not (null rest) -> return (test_fail (test_err ("Non-compliant action at step " ++ show (time state + 1)) test_result))
            | otherwise -> return test_result -- negative_test + rest == []
    [option]   -> case option of 
      Right act -> do
        state1 <- update_state spec state act
        (state2, powers, duties) <- normative_positions spec state1
        (state3, breaches) <- test_invariants spec state2 
        tester rest spec state3 positive_test (test_step act state3 powers duties breaches test_result)
      Left (QueryInfo True) -> tester rest spec (increment_time state) positive_test (test_repeat_step test_result)
      Left (QueryInfo False) -> tester rest spec (increment_time state) positive_test (test_fail (test_err ("Query at step " ++ show (time state + 1) ++ " failed") (test_repeat_step test_result)))
    options -> return (test_fail (test_ambig ("Multiple actions available at step " ++ show (time state + 1)) options test_result))

select_step_options :: Scenario -> Spec -> State -> IO ([Either QueryInfo ActionInfo], Scenario)
select_step_options [] spec state = do
  let op (state',res) (k,_) = fmap (res++) <$> enables_acts spec state' k
  (_, as) <- foldM op (state, []) (M.assocs (contents state))
  return (map Right as, [])
select_step_options (query@(Query t):rest) spec state = do 
  (_,holds) <- run_query spec state t 
  return ([Left (QueryInfo {query_success = holds})], rest)
select_step_options (stmt@(Action xs aty (Left term)):rest) spec state = 
  (, rest) . map Right . snd <$> mk_action spec state xs aty term
select_step_options (stmt@(Action xs t (Right (d,m))):rest) spec state =
  (, rest) . map Right . snd <$> mk_action spec state xs t (App d m)

mk_action spec state xs AddEvent t = create_create_act spec state xs t
mk_action spec state xs RemEvent t = create_terminate_act spec state xs t
mk_action spec state xs CallAct  t = enable_act spec state xs t

make_choice :: Spec -> [ActionInfo] -> IO (Maybe ActionInfo)
make_choice spec [option] = do
  putStrLn "Choosing the following option (only available):"
  putStrLn (show_action option)
  putStrLn "Press <ENTER> to continue"
  _ <- getLine 
  return (Just option)
make_choice spec options = do 
  putStrLn "Choose an act to perform (0 to quit): "
  show_options options
  input <- readLn -- TODO validate input
  if input == 0 then return Nothing
  else if input < 0 || input > length options then make_choice spec options
    else return (Just (options !! (input - 1)))

show_options :: [ActionInfo] -> IO ()
show_options options = 
  forM_ (zip [1..] options) $ \(i, option) -> do
    putStrLn (show i ++ ": " ++ show_action option)

print_state :: State -> IO ()
print_state state = print state

-- ### acts

action_actor :: ActionInfo -> Tagged
action_actor act = case action_tagged act of 
  (Product (actor:_),_) -> actor
  _                     -> error "act;actor assert"

show_action :: ActionInfo -> String
show_action act = show_component (action_tagged act) ++ effects
  where effects | action_type act /= CallAct = []
                | otherwise = concatMap ((" -"++) . show_component) (action_terminates act) ++ 
                              concatMap ((" +"++) . show_component) (action_creates act)


create_act :: (Tagged -> ActionInfo -> ActionInfo) -> Spec -> State -> [Var] -> Term -> IO (State, [ActionInfo])
create_act app spec state xs t = fmap concat <$> runSubs creater spec state emptySubs
  where creater = do
          taggeds <- foreach xs (whenTagged (eval t) return)
          return [app tagged $ ActionInfo { action_tagged = tagged
                                          , action_terminates = S.empty
                                          , action_creates = S.empty
                                          , action_type = CallAct } | tagged <- taggeds ]

create_create_act, create_terminate_act :: Spec -> State -> [Var] -> Term -> IO (State, [ActionInfo])
create_create_act = create_act (\t a -> a {action_terminates = S.empty, action_creates = S.singleton t, action_type = AddEvent})
create_terminate_act = create_act (\t a -> a {action_terminates = S.singleton t, action_creates = S.empty, action_type = RemEvent})

run_query :: Spec -> State -> Term -> IO (State, Bool)
run_query spec state t = do
  (s, vs) <- runSubs (eval t) spec state emptySubs  
  return (s, all (== (ResBool True)) vs)

data ActionInfo = ActionInfo {
                  action_tagged      :: Tagged 
                , action_terminates  :: S.Set Tagged
                , action_creates     :: S.Set Tagged
                , action_type        :: ActionType
                }

data QueryInfo  = QueryInfo {
                  query_success  :: Bool
                }

instance ToJSON ActionInfo where
  toJSON ai = object ["instance"    .= toJSON (TaggedJSON (action_tagged ai))
                     ,"creates"     .= toJSON (map TaggedJSON (S.toList (action_creates ai)))
                     ,"terminates"  .= toJSON (map TaggedJSON (S.toList (action_terminates ai)))
                     ]

data DutyInfo = DutyInfo {
                duty_name         :: DomId
              , duty_tagged       :: Tagged
              , duty_violated  :: Bool
              }

instance ToJSON DutyInfo where
  toJSON di = object ["instance"    .= toJSON (show_component (duty_tagged di))
                     ,"enforceable" .= toJSON (duty_violated di)
                     ]

test_invariants :: Spec -> State -> IO (State, [DomId])
test_invariants spec state = foldM op (state, []) (invariants spec)
  where op (state, acc) d = do
          (state', acc') <- runSubs (check d) spec state emptySubs
          return (state', acc ++ acc')
        check d = checkFalse (eval (Exists [Var d ""] (Present (Ref (Var d ""))))) >> return d

normative_positions :: Spec -> State -> IO (State, [ActionInfo], [DutyInfo])
normative_positions spec state = --return (state, [], [])
  let op (state0,as,ds) (te@(v,d),_) = case fromJust (fmap kind (find_decl spec d)) of
        Fact _ -> return (state0, as, ds)
        Duty dspec -> do 
          (st, vs) <- runSubs (eval_violation_condition te (violated_when dspec)) spec state emptySubs
          let di = DutyInfo { duty_name = d, duty_tagged = te, duty_violated = or vs }
          return (state0, as, di:ds)
        Act _  -> do (state1,as') <- enables_acts spec state0 te
                     return (state1, as' ++ as, ds)
  in foldM op (state, [], []) (M.assocs (contents state))

-- given a component, check whether it is in the given state and whether that component enables any acts 
enables_acts :: Spec -> State -> Tagged -> IO (State, [ActionInfo])
enables_acts spec state te = runSubs (instantiate_action te) spec state emptySubs

enable_act :: Spec -> State -> [Var] -> Term -> IO (State, [ActionInfo])
enable_act spec state xs t = fmap concat <$> runSubs creater spec state emptySubs
  where creater = foreach xs (whenTagged (eval t) instantiate_action)

instantiate_action :: Tagged -> M_Subs ActionInfo
instantiate_action te@(v,d) = do 
  is_in_state te >>= \case
    False -> empty
    True  -> get_kind d >>= \case 
      Fact _    -> empty
      Duty _    -> empty 
      Act aspec ->  do  (dom, _) <- get_dom d
                        let Products xs = dom
                        let Product args = v
                        modify_subs (`subsUnion` (M.fromList (zip xs args))) $ do
                          (es_t, es_c) <- foldM select_instances (S.empty, S.empty) (effects aspec)
                          results (checkTrue (eval (condition aspec))) >>= \case 
                            []  -> empty
                            _   -> return (ActionInfo te es_t es_c CallAct)
  where select_instances (es_t, es_c) eff = case eff of
          CAll xs t -> foreach xs (whenTagged (eval t) return) >>= \tes ->
                        return (es_t, S.fromList tes `S.union` es_c)
          TAll xs t -> foreach xs (whenTaggedHolds (eval t) return) >>= \tes ->
                        return (S.fromList tes `S.union` es_t, es_c)

fire_act :: Spec -> State -> ActionInfo -> State
fire_act spec state act = 
  do_creates (action_creates act) (do_terminates (action_terminates act) state)
  where 
        -- assumes the decomposition will result in consistency
        do_creates :: S.Set Tagged -> State -> State
        do_creates = flip (S.foldr create)

        -- likewise
        do_terminates :: S.Set Tagged -> State -> State
        do_terminates = flip (S.foldr terminate)

-- ### instantiation mechanisms

data TestResult = TestResult { 
                    success             :: Bool
                  , errors              :: [String]
                  , alternative_actions :: [String]
                  , present_powers      :: IM.IntMap [TaggedJSON] 
                  , present_duties      :: IM.IntMap [(TaggedJSON, Bool)]
                  , violated_invariants :: IM.IntMap [DomId]
                  , violated_duties     :: IM.IntMap [TaggedJSON]
                  , exec_actions        :: IM.IntMap (Maybe ActionInfo)
                  , reached_states      :: IM.IntMap State
                  , current_state       :: Int
                  } deriving (Generic) 

instance ToJSON TestResult where

empty_test_result :: Spec -> State -> [ActionInfo] -> [DutyInfo] -> [DomId] -> TestResult
empty_test_result spec state powers duties breaches = 
  test_step' Nothing state powers duties breaches $
  TestResult True [] [] IM.empty IM.empty IM.empty IM.empty IM.empty IM.empty (-1)

test_step :: ActionInfo -> State -> [ActionInfo] -> [DutyInfo] -> [DomId] -> TestResult -> TestResult 
test_step a = test_step' (Just a)
test_step' maction state powers duties breaches tr = 
  tr { exec_actions   = (if isNothing maction then id else IM.insert k maction) (exec_actions tr)
     , reached_states = IM.insert k state (reached_states tr)
     , present_powers = IM.insert k (map (TaggedJSON . action_tagged) powers) (present_powers tr)
     , present_duties = IM.insert k (map duty_res duties) (present_duties tr)
     , violated_duties = IM.insert k (map TaggedJSON viol_duties) (violated_duties tr)
     , violated_invariants = IM.insert k breaches (violated_invariants tr) 
     , errors = (errors tr) ++ add_errors
     , current_state = k }
 where k | isNothing maction = 0
         | otherwise         = length (exec_actions tr) + 1
       duty_res d = (TaggedJSON (duty_tagged d), duty_violated d)
       viol_duties = map duty_tagged (filter duty_violated duties) 
       add_errors = map (toDutyErr k . show_component) viol_duties ++
                    map (toInvErr k) breaches
        where toDutyErr k d = "Duty violated at step " ++ show k ++ "\n    " ++ d 
              toInvErr k i  = "Invariant violated at step " ++ show k ++ "\n    " ++ i

test_repeat_step :: TestResult -> TestResult
test_repeat_step tr = 
  tr { exec_actions = IM.insert k Nothing (exec_actions tr)
     , reached_states = IM.insert k (reached_states tr IM.! (k-1)) (reached_states tr)
     , present_powers = IM.insert k (present_powers tr IM.! (k-1)) (present_powers tr)
     , present_duties = IM.insert k (present_duties tr IM.! (k-1)) (present_duties tr)
     , violated_duties = IM.insert k (violated_duties tr IM.! (k-1)) (violated_duties tr)
     , violated_invariants = IM.insert k (violated_invariants tr IM.! (k-1)) (violated_invariants tr)
     , current_state = k
     }
  where k = length (exec_actions tr) + 1

test_err :: String -> TestResult -> TestResult
test_err str tr = tr {errors = (errors tr) ++ [str]}

test_fail :: TestResult -> TestResult
test_fail tr = tr { success = False }

test_ambig :: String -> [Either QueryInfo ActionInfo] -> TestResult -> TestResult
test_ambig str acts tr = (test_err str tr){ alternative_actions = map (either (const "<query>") show_action) acts} 
