{-# LANGUAGE OverloadedStrings #-}

import Spec hiding (Value(..))
import State
import StaticEval
import Parse
import Sim

import Control.Monad
import Control.Applicative

import Text.Read (readMaybe)
import Data.List (isSuffixOf, isPrefixOf)
import qualified Data.IntMap as IM

import System.IO (hReady, hGetLine, hPutStrLn, hClose, IOMode(ReadWriteMode))
import System.Environment (getArgs)
import Network.Socket 
import Network.Socket.ByteString 
import qualified Data.ByteString.Lazy.Char8 (pack,unpack)

import Data.Aeson hiding (String, Value(..))
import qualified Data.Aeson as JSON

main :: IO ()
main = do
  args <- getArgs 
  case args of 
    (f:p:opts) | ".eflint" `isSuffixOf` f && all ("--" `isPrefixOf`) opts 
               , Just port_nr <- readMaybe p -> do 
      fcont <- readFile f 
      case parse_flint fcont of
        Left err -> putStrLn "parse error:" >> putStrLn err
        Right (s, r, i, _) -> init_server opts port_nr s r i  
    _ -> putStrLn "please provide: <NAME>.eflint <PORT> <OPTIONS>"

init_server :: [String] -> PortNumber -> Spec -> Refiner -> Initialiser -> IO ()
init_server opts port_nr spec' ref' init' = do
  case compile_all spec' ref' init' [] of
    Left errs -> putStrLn "cannot compile specification" >> putStrLn (unlines errs)
    Right (spec', ref, init, _) -> do
      let spec = refine_specification spec' ref
      state <- make_initial_state spec init
      (state, tr) <- init_test_result spec state
      sock <- socket AF_INET Stream 0
      setSocketOption sock ReuseAddr 1
      Network.Socket.bind sock (SockAddrInet port_nr (tupleToHostAddress (127,0,0,1)))
      listen sock 2
      server opts sock spec state tr

server :: [String] -> Socket -> Spec -> State -> TestResult -> IO ()
server opts sock spec state tr = continue sock state tr
  where continue sock state tr = do
          putStrLn "--- AWAITING STATEMENT ---"
          conn <- accept sock
          handle <- socketToHandle (fst conn) ReadWriteMode
          string <- hGetLine handle
          putStrLn string
          let compile_and stmt c = case runStatic (compile_stmt stmt) spec of 
                  Left errs  -> do hPutStrLn handle (json_encode (InvalidInput (unlines errs))) >> hClose handle 
                                   continue sock state tr
                  Right stmt -> c stmt
          let report_success sock tr = do
                 let state_id = current_state tr
                 let violations = map DutyViolation (violated_duties tr IM.! state_id) ++
                                  map InvariantViolation (violated_invariants tr IM.! state_id)
                 hPutStrLn handle (json_encode (CommandSuccess state_id violations)) >> hClose handle
                 continue sock (reached_states tr IM.! state_id) (tr { errors = [], success = True })
          let do_event event_type term sock state tr = do
                let stmt = Action [] event_type (Left term)
                compile_and stmt $ \stmt -> do 
                  report_success sock =<< test_single_statement stmt spec state tr
          case eitherDecode (Data.ByteString.Lazy.Char8.pack string) of 
            Left err -> do when ("--debug" `elem` opts) (putStrLn err)
                           hPutStrLn handle (json_encode InvalidCommand) >> hClose handle
                           continue sock state tr 
            Right cmd-> case cmd of 
              CreateEvent term    -> do_event AddEvent term sock state tr
              TerminateEvent term -> do_event RemEvent term sock state tr
              QueryCommand term   -> 
                compile_and (Query term) $ \stmt -> do
                  tr <- test_single_statement stmt spec state tr
                  if success tr then report_success sock tr
                                else do hPutStrLn handle (json_encode QueryFailed) >> hClose handle
                                        continue sock state (tr { success = True, errors = [] })
              Revert new_state | new_state < 0 -> report_success sock (revert_state 0 tr)
                               | otherwise     -> case IM.lookup new_state (reached_states tr) of
                Nothing     -> do hPutStrLn handle (json_encode InvalidState) >> hClose handle
                                  continue sock state tr
                Just state  -> report_success sock (revert_state new_state tr) 
              ActionCommand d a r os -> compile_and (Action [] CallAct (Left term)) $ \stmt -> do
                tr <- test_single_statement stmt spec state tr
                if success tr then report_success sock tr
                              else do hPutStrLn handle (json_encode NonCompliantAction) >> hClose handle 
                                      continue sock state (tr {success = True, errors = []})
                where term = App d (Left (StringLit a : StringLit r : os))
                  
revert_state :: Int -> TestResult -> TestResult
revert_state s tr = tr  { current_state       = s
                        , exec_actions        = IM.filterWithKey (\k _ -> k <= s) (exec_actions tr)
                        , reached_states      = IM.filterWithKey (\k _ -> k <= s) (reached_states tr)
                        , present_powers      = IM.filterWithKey (\k _ -> k <= s) (present_powers tr)
                        , present_duties      = IM.filterWithKey (\k _ -> k <= s) (present_duties tr)
                        , violated_duties     = IM.filterWithKey (\k _ -> k <= s) (violated_duties tr)
                        , violated_invariants = IM.filterWithKey (\k _ -> k <= s) (violated_invariants tr)
                        }

json_encode r = Data.ByteString.Lazy.Char8.unpack (encode r)

data Command    = ActionCommand DomId DomId DomId [Term]
                | CreateEvent Term
                | TerminateEvent Term
                | QueryCommand Term
                | Revert Int 
                 

instance FromJSON Command where
  parseJSON = withObject "Command" $ \v -> do 
                cmd <- v .: "command"
                case cmd::String of 
                  "create"      -> CreateEvent . value_to_term <$> v .: "value"
                  "terminate"   -> TerminateEvent . value_to_term <$> v .: "value"
                  "test-present"-> QueryCommand . value_to_term <$> v .: "value"
                  "test-absent" -> QueryCommand . Not . value_to_term <$> v .: "value"
                  "revert"      -> Revert <$> v .: "value"
                  "action"      -> actionCommand <$> v .: "act-type"
                                                 <*> v .: "actor"
                                                 <*> v .: "recipient"
                                                 <*> v .: "objects"
                    where actionCommand d a r os = ActionCommand d a r (map value_to_term os)
                  _             -> mzero

data Value      = Atom DomId (Either String Int)
                | Composite DomId [Value]

value_to_term :: Value -> Term
value_to_term v = case v of
  Atom d (Left s)  -> App d (Left [StringLit s]) 
  Atom d (Right i) -> App d (Left [IntLit i])
  Composite d vs   -> App d (Left (map value_to_term vs))

value_to_tagged :: Value -> Tagged
value_to_tagged v = case v of
  Atom d (Left s)  -> (String s, d) 
  Atom d (Right i) -> (Int i, d)
  Composite d vs   -> (Product (map value_to_tagged vs), d)

instance FromJSON Value where
  parseJSON = withObject "Value" $ \v -> 
                    (\c i -> Atom c (Right i)) <$> v .: "fact-type" <*> v .: "value"
                <|> (\c s -> Atom c (Left s))  <$> v .: "fact-type" <*> v .: "value"
                <|> Composite <$> v .: "fact-type" <*> v .: "value"

data Response   = InvalidCommand
                | CommandSuccess Int [Violation]
                | InvalidInput String -- compilation error
                | NonCompliantAction
--                | NoCompliantAction -- non-deterministic, none enabled
--                | MultipleCompliantAction -- non-deterministic, more than 1 enabled 
                | QueryFailed
                | InvalidState

data Violation  = DutyViolation TaggedJSON 
                | InvariantViolation DomId 

instance ToJSON Response where
  toJSON InvalidCommand = object [ "response"   .= JSON.String "invalid command" ]
  toJSON (CommandSuccess i vs)  = object [ "response"   .= JSON.String "success"
                                         , "violations" .= toJSON vs
                                         , "new-state"  .= toJSON i ]
  toJSON (InvalidInput err) = object [ "response" .= JSON.String "invalid input" 
                                     , "error"    .= toJSON err ]
  toJSON NonCompliantAction = object [ "response" .= JSON.String "non-compliant action" ]
  toJSON QueryFailed        = object [ "response" .= JSON.String "query failed" ]
  toJSON InvalidState       = object [ "response" .= JSON.String "invalid state" ]

instance ToJSON Violation where
  toJSON (DutyViolation te) = 
    object [ "violation" .= toJSON ("duty"::String)
           , "value"     .= toJSON te ]
  toJSON (InvariantViolation d) = 
    object [ "violation" .= toJSON ("invariant"::String)
           , "invariant" .= toJSON d ]

