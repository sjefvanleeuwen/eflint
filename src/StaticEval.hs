{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module StaticEval where

import Spec
import Binders

import Control.Monad
import Control.Applicative

import Data.List ((\\), elem)
import qualified Data.Map as M
import qualified Data.Set as S

data M_Stc a = M_Stc {runStatic :: Spec -> Either [String] a}

instance Monad M_Stc where
  return a = M_Stc (const (Right a))
  m >>= f  = M_Stc $ \spec -> case runStatic m spec of 
                                Right a  -> runStatic (f a) spec
                                Left err -> Left err

instance Applicative M_Stc where
  pure  = return
  (<*>) = ap

instance Functor M_Stc where
  fmap  = liftM

instance Alternative M_Stc where
  empty   = M_Stc (const (Left []))
  p <|> q = M_Stc $ \spec -> case runStatic p spec of 
                              Left ers1 -> case runStatic q spec of
                                            Left ers2 -> Left (ers1 ++ ers2)
                                            Right a   -> Right a 
                              Right a   -> Right a 

err :: String -> M_Stc a
err str = M_Stc $ \spec -> Left [str]

get_dom :: DomId -> M_Stc Domain
get_dom d = M_Stc $ \spec -> case find_decl spec d of 
                              Just tspec  -> Right (domain tspec)
                              _           -> Left ["undeclared type: " ++ d]

get_spec :: M_Stc Spec
get_spec = M_Stc Right 

with_spec :: Spec -> M_Stc a -> M_Stc a
with_spec spec m = M_Stc $ \_ -> runStatic m spec

free_vars :: Term -> M_Stc (S.Set Var)
free_vars t = do spec <- get_spec 
                 return (free spec t)

compile_all :: Spec -> Refiner -> Initialiser -> Scenario -> Either [String] (Spec, Refiner, Initialiser, Scenario) 
compile_all f r i s = case runStatic compile f of
  Left errs                -> Left errs  
  Right (spec, init, scen) -> Right (spec, r, init, scen)
  where compile = do
          f' <- compile_spec
          with_spec f' $ do
            i' <- compile_initialiser i
            s' <- compile_stmts s
            return (f',i',s')

compile_initialiser :: Initialiser -> M_Stc Initialiser
compile_initialiser = mapM (compile_effect [])

compile_stmts :: [Statement] -> M_Stc [Statement]
compile_stmts = mapM compile_stmt

compile_stmt :: Statement -> M_Stc Statement
compile_stmt (Query t0) = do
  fs <- free_vars t0
  let unbounds = S.toList fs
  let t1 | null unbounds  = t0
         | otherwise      = Forall unbounds t0
  t' <- convert_term t1 TyBool
  return (Query t')
compile_stmt (Action xs aty (Right (d,mods))) = compile_action_term xs aty (App d mods) (Just d)
compile_stmt (Action xs aty (Left t)) = compile_action_term xs aty t Nothing
compile_action_term xs aty term md = do 
  fs <- free_vars term
  let unbounds = S.toList (fs `S.difference` S.fromList xs)
  Action (xs ++ unbounds) aty . Left <$> converter term
  where converter term = case md of Just d -> convert_term term (TyTagged d)
                                    _      -> fst <$> compile_term term

compile_spec :: M_Stc Spec
compile_spec = do 
  spec <- get_spec 
  ds   <- sequence (map compile_pair (M.toList (decls spec)))
  let invs = concatMap op ds
        where op (d,tspec) = case kind tspec of 
                 Fact f | invariant f -> [d]
                 _                    -> []
  return (spec {decls = M.fromList ds, invariants = invs})
  where compile_pair :: (DomId, TypeSpec) -> M_Stc (DomId, TypeSpec)
        compile_pair (d,tspec) = do
          derivation' <- case derivation tspec of Nothing -> return Nothing
                                                  Just dv -> Just <$> compile_derivation xs d dv
                                                    where xs = case domain tspec of 
                                                                Products xs -> xs
                                                                _           -> []
          kind' <- case kind tspec of 
                    Act aspec -> do 
                      let Products xs = domain tspec
                      -- convert pre-condition
                      cond' <- convert_term (condition aspec) TyBool
                      cond_fs <- free_vars cond'
                      let cond'' | null unbounds  = cond'
                                 | otherwise      = Exists unbounds cond'
                            where unbounds = S.toList (cond_fs `S.difference` S.fromList xs)
                      -- convert post-conditions
                      effects' <- sequence (map (compile_effect xs) (effects aspec))
                      -- build new act
                      return (Act (aspec { condition = cond'', effects = effects' } ))
                    Duty dspec -> 
                      case violated_when dspec of 
                        Just term -> compile_condition term
                        Nothing -> do -- TODO, should be dealt with by outer -> inner translation
                                      conds <- mapM select_condition (maybe [] id (enforcing_acts dspec))
                                      compile_condition (foldr Or (BoolLit False) conds)
                         where select_condition a = do
                                 spec <- get_spec
                                 case fmap kind (M.lookup a (decls spec)) of 
                                    Just (Act aspec) -> return (condition aspec) 
                                    _ -> err ("duty " ++ d ++ " is declared with enforcing act " ++ a ++ " which is not a declared or not declared as an act")
                     where  compile_condition term = do
                               let Products xs = domain tspec
                               term' <- convert_term term TyBool
                               fs <- free_vars term'
                               let unbounds = S.toList (fs `S.difference` (S.fromList xs))
                                   term'' | null unbounds = term'
                                          | otherwise     = Exists unbounds term'
                               return (Duty (dspec { violated_when = Just term'' }))
                    _         -> return (kind tspec)
          dom_filter <- convert_term (domain_constraint tspec) TyBool
          return (d, tspec { derivation = derivation', kind = kind', domain_constraint = dom_filter })
          
compile_derivation :: [Var] -> DomId -> Derivation -> M_Stc Derivation
compile_derivation bound d ExternalValue = return ExternalValue
compile_derivation bound d (Dv xs t) = do
  t' <- convert_term t (TyTagged d)
  fs <- free_vars t'
  let unbounds = S.toList (fs `S.difference` (S.fromList xs))
  return (Dv (xs ++ unbounds) t')

compile_primitive_application :: DomId -> Arguments -> M_Stc (Term, Type)
compile_primitive_application d params = get_dom d >>= \dom -> case (dom, params) of
  (_, Left as) | length as > 1 -> err error_string
  (External, Left [])    -> primitive d (StringLit "()") (Just TyStrings)
  (External, Right [])   -> primitive d (StringLit "()") (Just TyStrings)
  (External, Left [a])   -> primitive d a Nothing 
--  (AnyString, Left [])   -> primitive d (StringLit "()") (Just TyStrings)
--  (AnyString, Right [])  -> primitive d (StringLit "()") (Just TyStrings)
  (AnyString, Left [a])  -> primitive d a (Just TyStrings)
  (Strings [s], Left []) -> primitive d (StringLit s) (Just TyStrings)
  (Strings [s], Right [])-> primitive d (StringLit s) (Just TyStrings)
  (Strings _, Left [a])  -> primitive d a (Just TyStrings)
  (AnyInt, Left [a])     -> primitive d a (Just TyInts)
  (Ints [i], Right [])   -> primitive d (IntLit i) (Just TyInts)
  (Ints [i], Left [])    -> primitive d (IntLit i) (Just TyInts)
  (Ints _, Left [a])     -> primitive d a (Just TyInts)
  (_, Right _)           -> err error_string
  _                      -> err error_string
  where error_string = "The constructor " ++ d ++ " is primitive, and should receive exactly one argument in functional style"
        primitive d t Nothing = do (t', _) <- compile_term t
                                   return (Tag t' d, TyTagged d)
        primitive d t (Just ty) = do t' <- convert_term t ty
                                     return (Tag t' d, TyTagged d)
        
 
compile_arguments :: DomId -> Arguments -> M_Stc Arguments
compile_arguments _ (Right ms) = Right <$> compile_modifiers ms
compile_arguments d (Left ts) = do
  spec <- get_spec
  dom <- get_dom d
  case dom of Products xs 
               | length xs == length ts -> Right <$> compile_modifiers (map (uncurry Rename) (zip xs ts))
               | otherwise -> err ("elements of " ++ d ++ " have " ++ show (length xs) ++ " components, " ++ show (length ts) ++ " given")
              External | null ts -> return (Right [])
              _ -> err ("non-composite " ++ d ++ " used in application")

compile_modifiers :: [Modifier] -> M_Stc [Modifier]
compile_modifiers mods = do
  spec <- get_spec
  let compile_mod (Rename x t) = Rename x <$> convert_term t (TyTagged (remove_decoration spec x)) 
  sequence (map compile_mod mods)

compile_effect :: [Var] -> Effect -> M_Stc Effect
compile_effect bound eff = case eff of 
  TAll xs t -> compile_effect' TAll xs t
  CAll xs t -> compile_effect' CAll xs t
  where compile_effect' cons xs t = do (t',tau) <- compile_term t
                                       fs <- free_vars t'
                                       let unbounds = S.toList (fs `S.difference` S.fromList (bound ++ xs))
                                       return (cons (xs ++ unbounds) t')

compile_term :: Term -> M_Stc (Term, Type)
compile_term t0 = do
 spec <- get_spec 
 case t0 of
  CurrentTime -> return (CurrentTime, TyInts)
  StringLit s -> return (t0, TyStrings)
  IntLit i    -> return (t0, TyInts)
  BoolLit b   -> return (t0, TyBool)
  Ref x       -> return (t0, TyTagged (remove_decoration spec x))
  App d ms    -> do case fmap domain (find_decl spec d) of 
                      Just (Products _) -> do  
                        ms' <- compile_arguments d ms 
                        return (App d ms', TyTagged d)
                      _ -> compile_primitive_application d ms 
  Tag t d     -> err "tagging is an auxiliary operation" 
  Untag t     -> err "untagging is an auxiliary operation"

  Not t       -> do t' <- convert_term t TyBool
                    return (Not t', TyBool)
  And t1 t2   -> do t1' <- convert_term t1 TyBool
                    t2' <- convert_term t2 TyBool
                    return (And t1' t2', TyBool)
  Or  t1 t2   -> do t1' <- convert_term t1 TyBool
                    t2' <- convert_term t2 TyBool
                    return (Or t1' t2', TyBool)

  Geq t1 t2   -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Geq t1' t2', TyBool)
  Leq t1 t2   -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Leq t1' t2', TyBool)
  Le t1 t2    -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Le t1' t2', TyBool)
  Ge t1 t2    -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Ge t1' t2', TyBool)

  Mult t1 t2  -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Mult t1' t2', TyInts)
  Div t1 t2  ->  do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Div t1' t2', TyInts)
  Sub t1 t2   -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Sub t1' t2', TyInts)
  Add t1 t2   -> do t1' <- convert_term t1 TyInts
                    t2' <- convert_term t2 TyInts
                    return (Add t1' t2', TyInts)
  Sum xs t    -> do t'  <- convert_term t TyInts 
                    return (Sum xs t', TyInts)

  Eq t1 t2    -> (do  (t1', tau1) <- compile_term t1
                      t2' <- convert_term t2 tau1
                      return (Eq t1' t2', TyBool)) <|>
                 (do  (t2', tau2) <- compile_term t2
                      t1' <- convert_term t1 tau2
                      return (Eq t1' t2', TyBool))
  Neq t1 t2   -> (do  (t1', tau1) <- compile_term t1
                      t2' <- convert_term t2 tau1
                      return (Neq t1' t2', TyBool)) <|>
                 (do  (t2', tau2) <- compile_term t2
                      t1' <- convert_term t1 tau2
                      return (Neq t1' t2', TyBool))
  Count xs t  -> do (t', tau) <- compile_term t
                    return (Count xs t', TyInts)
  When t1 t2  -> do (t1', tau) <- compile_term t1
                    t2' <- convert_term t2 TyBool
                    return (When t1' t2', tau)
  Present t   -> do (t', tau) <- compile_term t -- simplification, assuming no conversion to tagged-elems yet
                    case tau of TyTagged d  -> return (Present t', TyBool) 
                                errt        -> err ("Holds(t) requires t to evaluate to a an instance, instead got: " ++ show errt)
  Violated t  -> do (t', tau) <- compile_term t -- simplification, as above
                    case tau of TyTagged d -> return (Violated t', TyBool)
                                errt       -> err ("Violated(t) requires t to evaluate to an instance of a duty-type" ++ show errt)
  Enabled t   -> do (t', tau) <- compile_term t -- simplification, as above
                    case tau of TyTagged d -> return (Enabled t', TyBool)
                                errt       -> err ("Enabled(t) requires t to evaluate to an instance of a act-type" ++ show errt)
  Exists xs t -> do t' <- convert_term t TyBool
                    return (Exists xs t', TyBool)
  Forall xs t -> do t' <- convert_term t TyBool
                    return (Forall xs t', TyBool)
  Project t x -> do (t',tau) <- compile_term t
                    case tau of 
                      TyTagged d -> do 
                        dom <- get_dom d
                        case dom of 
                          Products xs ->  if elem x xs 
                                          then return (Project t' x, TyTagged (remove_decoration spec x))
                                          else err ("project(t,x) expects t to evaluate to a value of a product-type containing a reference to " ++ show x ++ " instead got: " ++ show xs) 
                          _ -> err ("project(t,x) expects t to evaluate to a value of a product-type, instead got: " ++ show dom) 
                      _ -> err ("project(t,x) expects t to evaluate to a tagged-element, instead got: " ++ show tau) 

-- term => term : type
convert_term :: Term -> Type -> M_Stc Term
convert_term t ty = do
  (t', ty') <- compile_term t
  if ty' == ty then return t'   -- rule id
               else case ty of
    TyStrings -> case ty' of TyTagged d -> flip match_domain AnyString <$> get_dom d >>= \case
                                            True  -> return (Untag t')
                                            False -> conv_err ty' ty
                             _ -> conv_err ty' ty
    TyInts -> case ty' of TyTagged d  -> flip match_domain AnyInt <$> get_dom d >>= \case
                                          True  -> return (Untag t')
                                          False -> conv_err ty' ty 
                          _           -> conv_err ty' ty
    TyBool -> case ty' of TyInts      -> return $ Geq t' (IntLit 1) -- rule INT -> BOOL
                          TyTagged _  -> return $ Present t' 
                          _           -> conv_err ty' ty
    TyTagged d -> get_dom d >>= match_type ty' >>= \case True  -> return (Tag t' d)
                                                         False -> conv_err ty' ty
  where conv_err source target = err ("cannot convert " ++ show source ++ " to " ++ show target)

match_type :: Type -> Domain -> M_Stc Bool
match_type (TyTagged d) dom = get_dom d >>= return . flip match_domain dom 
match_type TyInts dom = case dom of
  External    -> return True
  AnyInt      -> return True
  Ints _      -> return True
  Time        -> return True
  Strings _   -> return False
  AnyString   -> return False
  Products _  -> return False
match_type TyStrings dom = case dom of
  External    -> return True
  AnyString   -> return True
  Strings _   -> return True
  AnyInt      -> return False
  Ints _      -> return False
  Products _  -> return False
  Time        -> return False
match_type _ _ = return False

-- | second domain is the conversion target
match_domain :: Domain -> Domain -> Bool
match_domain dom dom' = case (dom, dom') of
  (AnyString, AnyString)    -> True 
  (Strings _, AnyString)    -> True
  (AnyString, Strings _)    -> False
  (Strings s1, Strings s2)  -> null (s1 \\ s2) 
  (AnyString, External)     -> True
  (Strings _, External)     -> True
  (AnyString, _)            -> False
  (Strings _, _)            -> False

  (AnyInt, AnyInt)          -> True
  (Ints _, AnyInt)          -> True
  (AnyInt, Ints _)          -> True
  (Ints i1, Ints i2)        -> null (i1 \\ i2)
  (AnyInt, AnyString)       -> False
  (AnyInt, Strings _)       -> False
  (AnyInt, Products _)      -> False
  (Ints _, AnyString)       -> False
  (Ints _, Strings _)       -> False
  (Ints _, Products _)      -> False
  (AnyInt, Time)            -> False
  (Ints _, Time)            -> False
  (AnyInt, External)        -> True
  (Ints _, External)        -> True
  
  (Time, Time)              -> True
  (Time, AnyInt)            -> True
  (Time, External)          -> True
  (Time, Ints _)            -> False
  (Time, AnyString)         -> False
  (Time, Strings _)         -> False
  (Time, Products _)        -> False

  (Products r, Products r') -> r == r'
  (Products _, _)           -> False

  (External, AnyInt)        -> True
  (External, Ints _)        -> True
  (External, Strings _)     -> True
  (External, AnyString)     -> True
  (External, Time)          -> True
  (External, External)      -> True
  (External, Products _)    -> False
