{-# LANGUAGE LambdaCase #-}

module Saturation where

import Spec
import State
import Eval

import Control.Monad

import qualified Data.Map as M
import qualified Data.Set as S

rebase_and_sat :: Spec -> State -> IO State
rebase_and_sat spec = saturate spec . rebase spec 

rebase :: Spec -> State -> State
rebase spec s = s { contents = M.filterWithKey op (contents s) }
  where op (_,d) _ = d `S.member` basic spec

saturate :: Spec -> State -> IO State
saturate spec state = saturate' spec state >>= \case
                        state' | state == state' -> return state
                               | otherwise       -> saturate spec state'
 where 
  saturate' spec s = foldM op s (S.toList (derived spec))
    where op s d = case join (fmap derivation (find_decl spec d)) of 
                    Nothing -> return s
                    Just ExternalValue -> return s
                    Just (Dv xs t) | any (isExternal spec) xs -> return s 
                                   | otherwise -> do
                      let dyn  = foreach xs (whenTagged (eval t) return)
                      (state', ress) <- runSubs dyn spec s M.empty
                      return $ create_all (concat ress) state'

