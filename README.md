## eFLINT 

Haskell implementation of the eFLINT language, providing an interpreter for exploring normative behaviour, testing normative models and interacting with a normative model for runtime verification and simulation.

### Installation

Requires GHC, the glorius Glasgow Haskell Compiler, and Cabal, Haskell's old skool package manager. On unix:

```
apt-get install cabal-install ghc
cabal update
```

Then in your local copy of this directory:

```
cabal install --dependencies
cabal build
```

The above assumes the old version of building Cabal packages; in newer versions of Cabal use `cabal v1-build`.

Upon successful installation:

* the executable is found at `dist/build/eflint-server/eflint-server`
* the script `examples/run_tests.sh` should run successfully and produce no output.

### Executable `eflint-server`

#### Usage

`eflint-server <FILE.eflint> <PORT> <OPTIONS*>`

with `<OPTIONS>` either:
 
* `--debug` (increases verbosity of the server)

#### Protocol

The `eflint-server` listens to commands on the given `<PORT>`. 
Any input that does not follow the format discussed below will be rejected by the server, responding with

```
{
    response  : "invalid command"
}    
``` 

If a command is executed successfully this might result in the server updating its internal state.
If this is the case then the response will contain a field `newstate : <INTEGER>`.

Valid commands have one of the following forms.

**Actions**

```
{
    command     : "action"
    act-type    : <STRING>
    actor       : <STRING>
    recipient   : <STRING>
    objects     : <VALUE*>
}
```

A `<VALUE>` is either an `<ATOM>` of the form

```
{
    fact-type : <STRING>
    value     : <STRING>|<INTEGER>
}
```

or a `<COMPOSITE>` of the form

```
{
    fact-type : <STRING>
    arguments : <VALUE*>
}
```

and `<VALUE*>` is an array of values.

The response to an action-command is an object of one of the following forms.

```
{
    response   : "success"
    new-state  : <INTEGER>
    violations : <VIOLATION*>
}
{
    response : "non-compliant action"
}
{
    response : "invalid input"
    error    : <STRING>
}
```

Where <VIOLATION*> is an array of elements of the form:

```
{
    violation : "duty"
    value     : <VALUE>
}
{
    violation : "invariant"
    invariant : <STRING>
}
```

An invalid action may occur if the fields do not constitute a valid action according to the server's normative model (e.g. based on type-checking).
The error contains a message indicating what went wrong (structure not specified).

**Events**

```
{
    command   : "create"
    value     : <VALUE>
}
{
    command   : "terminate"
    value     : <VALUE>
}
```

An event either fails because the provided value is not a value in the normative model or succeeds, resulting in a new state:

```
{
    response    : "success"
    new-state   : <INTEGER>
    violations  : <VIOLATION*>
}
{
    response  : "invalid input"
    error     : <STRING>
}
```

**Queries**

```
{
    command   : "test-present"
    value     : <VALUE>
}
{
    command   : "test-absent"
    value     : <VALUE>
}
```
A query either fails because it is not present (or not absent), fails because the provided value is not a value in the normative model, or succeeds (remaining in the same state):

```
{
    response  : "success"
}
{
    response   : "query failed"
}
{
    response  : "invalid input"
    error     : <STRING>
}
```

**Backtracking**

The `eflint-server` can backtrack to one of its previous states (configurations), triggered by a command of the form:

```
{
    command   : "revert"
    value     : <INTEGER>
}
```

The provided `<INTEGER>` should be a positive number previously been sent as part of a response or be a negative number, in which case the server will revert to its initial state.

The response is of the form:

```
{
    response   : "success"
    new-state  : <INTEGER>
    violations : <VIOLATION*>
}
{
    response   : "invalid state"
}
```