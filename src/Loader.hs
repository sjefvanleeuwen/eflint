{-# LANGUAGE LambdaCase #-}

module Loader where

import GLL.Combinators
import Text.XML.HXT.Core 

import Control.Monad (foldM)

import Data.List (intercalate)

import Spec
import Parse

loader :: String -> IOStateArrow s a XmlTree
loader src = readDocument [] src >>> removeDocWhiteSpace

parser_value_expr :: IOStateArrow s XmlTree Expr
parser_value_expr = 
  listA (hasName "value-expr" >>> getChildren >>> getText) >>> arrIO maybe_parse_expr >>> unlistA
  where maybe_parse_expr = foldM attempt []
          where attempt ress str = case parse_value_expr str of
                                    [res] -> return (res:ress)
                                    _ -> putStrLn ("invalid value-expr: " ++ str) >> return ress

parser_type_expr :: IOStateArrow s XmlTree Domain 
parser_type_expr = parser_type_variant ["type-expr"] type_expr 

parser_type :: IOStateArrow s XmlTree DecType 
parser_type = parser_type_variant simple_type_names decorated_type 

parser_type_ref :: IOStateArrow s XmlTree TypeRef
parser_type_ref = parser_type_variant simple_type_names type_ref

parser_type_name :: IOStateArrow s XmlTree Type
parser_type_name = parser_type_variant simple_type_names id_lit

simple_type_names = ["type", "iact-type", "iduty-type", "ifact-type"]

parser_type_variant :: [String] -> BNF Token a -> IOStateArrow s XmlTree a
parser_type_variant names parser = 
  listA (right_name >>> getChildren >>> getText) >>> arrIO maybe_parse_expr >>> unlistA
  where maybe_parse_expr = foldM attempt []
         where attempt ress str = case flint_parser parser str of
                                   [res]  -> return (res:ress)
                                   _      -> putStrLn ("invalid type (" ++ show_names ++") : " ++ str) >> return ress
        right_name = foldr check zeroArrow names
          where check n rest = hasName n <+> rest
        show_names = intercalate "," names
